// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="run-operator-remotely"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Running the Open Data Hub Operator remotely
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

The standard procedure for developing and testing new features for the operator is

. Make code changes
. Build the operator image
. Push the changes to an image repository like https://quay.io
. Deploy the operator image to an OpenShift project
. Test that new features work correctly
. Repeat steps as needed

With the operator-sdk we can run the operator remotely (outside of the OpenShift cluster) on our local development machines in real time without needing to build and push an operator container image.

.Prerequisites
The same prerequisites as listed on the official https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/user-guide.md[Ansible Operator SDK user guide] for https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/user-guide.md#2-run-outside-the-cluster[running the operator outside of the cluster]

* https://docs.openshift.com/container-platform/3.11/cli_reference/get_started_cli.html#installing-the-cli[Openshift cli]
* https://github.com/operator-framework/operator-sdk[Operator SDK]
* You will need to follow the link:manual-installation.adoc[manual installation docs] to setup the OpenShift project and create the ODH service account. Since you are running the operator remotely, you will need to follow the instructions until you get to the step that deploys the operator.
* Python virutal environment and package manager such as https://github.com/pypa/pipenv[pipenv]
* Python packages:
** ansible
** ansible-runner
** ansible-runner-http
** openshift
* base64 command (or any command that will decode base64 encoded OpenShift secrets)

.Procedure

. Once the project and service account have been created, you will need to get the authentication token for the opendatahub-operator service account.
  This is a necessary step so that you can confirm that the remote operator is using the same permissions that the cluster local operator pod would use.
.. Log into the OpenShift cluster as a user with `admin` access to the project.
+
  $ oc login <OpenShift API Endpoint>

.. You will want to log into the OpenShift cluster as the `opendatahub-operator` service account. For convenience and since the service account authentication token doesn't expire, you will want to store it in an environment variable so you can retrieve it at anytime
+
 $ export ODH_OPERATOR_SERVICE_ACCOUNT_TOKEN=`oc sa get-token opendatahub-operator`

. Log into the OpenShift cluster with the token of the the service account that the operator will be running under
+
 $ oc login <OpenShift API Endpoint> --token $ODH_OPERATOR_SERVICE_ACCOUNT_TOKEN

. From the root of the opendatahub-operator git repository, setup your virtual environment for Python package management
+
 $ pipenv shell

. Install the required Python packages
+
 $ pipenv install ansible ansible-runner ansible-runner-http openshift

. Since the `watches.yaml` file states that the `playbook.yaml` file will be located in `/opt/ansible`, you will need to make sure that playbook.yaml and the roles directory is accessible from that directory.
  Instead of copying the files to `/opt/ansible` after every change, you can create a symlink to both objects to ensure that the latest version is always available
+
....
 # From the root of the opendatahub-operator git repository

 # Ensure that the directory exists
 $ sudo mkdir -p /opt/ansible

 # Create symlinks that are relative to /opt/ansible
 $ sudo ln -s $PWD /opt/ansible
....

. Execute the operator-sdk command so that the operator runs locally
+
 $ operator-sdk up local


The operator-sdk will load the ansible files and start watching the cluster based on the permissions for the currently authenticated OpenShift user.
Once the operator is running remotely it will function exactly as it would if it were running inside a pod within the cluster.
You can view the ansible output of the most recent playbook execution at

 /tmp/ansible-operator/runner/opendatahub.io/v1alpha1/OpenDataHub/opendatahub/<ODH CUSTOM RESOURCE NAME>/artifacts/latest/stdout

//.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.

.Additional resources

* https://github.com/operator-framework/operator-sdk[Operator SDK]
* https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/user-guide.md[Ansible Operator User Guide]
* https://docs.openshift.com/container-platform/3.11/dev_guide/service_accounts.html#dev-managing-service-accounts[OCP 3.11 Developer Guide - Managing Service Accounts]
