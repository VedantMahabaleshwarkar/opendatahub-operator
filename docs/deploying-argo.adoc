// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="deploying-argo"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Deploying Argo Setup
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

This procedure describes how to enable (and disable) Argo for the Open Data Hub

.Prerequisites

* A terminal shell with the OpenShift client command `oc` available.
* A copy of the files in this repository available in your terminal shell.
* The ability to run the operator as outlined at link:manual-installation.adoc[manual-installation]


.Procedure

. Change directory to your copy of the repository.
. Open `deploy/crds/opendatahub_v1alpha1_opendatahub_cr.yaml` in your editor of choice.
. Setting `odh_deploy` to `true` or `false` will either enable or disable a component. Edit the following lines in the file to do so.
+
....
  argo:
    odh_deploy: true
....

//.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.
To verify installation is successful, check all Argo pods (workflow-controller and argo-ui) are running with no errors. Users can start deploying workflows after they are given the following permissions

* edit file `deploy/argo-user-rbac.yaml` to include the correct namespace
* `oc create -f deploy/argo-user-rbac.yaml -n <namespace>`
* `oc adm policy add-role-to-user argo-user <username> -n <namespace>`
* `oc create -f https://raw.githubusercontent.com/argoproj/argo/master/examples/hello-world.yaml -n <namespace>`
* `oc get pods -n <namespace>`
* from the openshift portal visit the argo-ui route url to check the Argo portal

The hello world example workflow does not require any specific resources. For workflows that require more resources such as reading a secret, the workflow spec must specify a service account in the field *workflow.spec.serviceAccountName*. A specific role that allows access to the resource must be bound to that service account. For an example configuration file that creates and binds to a service account please install the example provided which is based on an example provided by link:https://argoproj.github.io/docs/argo/docs/workflow-rbac.html[Argo]:

* `oc create -f deploy/argo-workflow-rbac.yaml`

.Additional resources

* More information about *Argo* can be found  at link:https://argoproj.github.io/[https://argoproj.github.io/].

