Superset 
=========

The role will deploy Apache [Superset](https://github.com/apache/incubator-superset). 

Dependencies
------------

There are no dependencies, however any external sql database needs to be already available if it's supplied with the CR for superset metadata. You will need to ensure that the SQL DB user (which you provided to `superset`) has `SELECT` permissions within the particular database and tables you will be connecting to and querying. 

Role Variables
--------------

All variables are expected to be located under `superset` in the Open Data Hub custom resource `.spec`

* `odh_deploy` - Set to `true` if you want to deploy superset with the ODH deployment
* `image` - Superset source image name
* `superset_admin` - Contains superset app admin credentials/info, subfields are `admin_usr`, `admin_psw`, `admin_fname`, `admin_lname`, `admin_email`
* `secret_key` - App secret key
* `data_volume_size` - The size of the data volume where you would mount your SQLite file (if you are using that as your backend), or a volume to collect any logs that are routed there
* `sqlalchemy_db_uri` - SQL DB URI for superset metadata, if not provided SQL Lite is used

Sample Configuration
--------------------

```
  superset:
    odh_deploy: true
    image: quay.io/aiops/superset:v0.30
    superset_admin:
      admin_usr: userKPJ
      admin_psw: 7ujmko0
      admin_fname: admin
      admin_lname: admin
      admin_email: admin@fab.org
    secret_key: thisISaSECRET_1234
    data_volume_size: 512Mi
    sqlalchemy_db_uri: sqlite:////var/lib/superset/superset.db

```